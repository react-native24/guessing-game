import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, Button, Image } from 'react-native';

import Colors from '../constants/colors';

const GameOverScreen = (props: any) => {
  return (
    <View style={styles.screen}>
      <Image style={styles.image} source={require('../assets/game_over.png')} />
      <Text style={styles.gameOverText}>The Game is over!</Text>
      <Text>Number of rounds: {props['roundsNumber']}</Text>
      <Text>Number was: {props['userNumber']}</Text>
      <View style={styles.buttonContainer}>
        <Button
          title='NEW GAME'
          onPress={props.onNewGame}
          color={Colors.primary}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 90,
    height: 90,
    padding: 100,
  },
  gameOverText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: Colors.secondary,
  },
  buttonContainer: {
    marginTop: 20,
  },
});

export default GameOverScreen;
