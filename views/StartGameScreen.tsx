import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from 'react-native';

import Card from '../components/Card';
import Colors from '../constants/colors';
import Input from '../components/Input';
import NumberContainer from '../components/NumberContainer';
import colors from '../constants/colors';

const StartGameScreen = (props: any) => {
  const [value, setValue] = useState('');
  const [confirmed, setConfirmed] = useState(false);
  const [selectedNumber, setSelectedNumber] = useState();
  const inputHandler = (inputText: string) => {
    setValue(inputText.replace(/[^0-9]/g, ''));
  };

  const resetInputHandler = () => {
    setValue('');
    setConfirmed(false);
  };

  const confirmInputHandler = () => {
    const choseNumber: any = parseInt(value);
    if (isNaN(choseNumber) || choseNumber <= 0 || choseNumber > 99) {
      Alert.alert(
        'Invalid number',
        'Number has to be a number between 1 and 99.',
        [{ text: 'Okay', style: 'destructive', onPress: resetInputHandler }]
      );
      return;
    }
    setConfirmed(true);
    setSelectedNumber(choseNumber);
    setValue('');
  };

  let confimedOutput;

  if (confirmed) {
    confimedOutput = (
      <Card style={styles.chosenNumber}>
        {/* <Text>Chosen number: {selectedNumber}</Text> */}
        <NumberContainer>{selectedNumber}</NumberContainer>
        <Button
          title='Start Game'
          onPress={() => props.onStartGame(selectedNumber)}
          color={Colors.tertiary}
        />
      </Card>
    );
  }

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.screen}>
        <Text style={styles.title}>Start a new game</Text>
        <Card style={styles.inputContainer}>
          <Text>Select a number</Text>
          <Input
            style={styles.input}
            keyboardType='number-pad'
            autoCorrect={false}
            maxLength={3}
            blurOnSubmit
            onChangeText={inputHandler}
            value={value}
          />
          <View style={styles.buttonContainer}>
            <View style={styles.button}>
              <Button
                title='Reset'
                onPress={resetInputHandler}
                color={Colors.primary}
              />
            </View>
            <View style={styles.button}>
              <Button
                title='Confirm'
                onPress={confirmInputHandler}
                color={Colors.secondary}
              />
            </View>
          </View>
        </Card>
        {confimedOutput}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  inputContainer: {
    padding: 15,
    borderRadius: 10,
    width: '80%',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
  },
  button: {
    width: '30%',
  },
  input: {
    width: '20%',
    textAlign: 'center',
    borderBottomWidth: 2,
    borderBottomColor: Colors.primary,
    fontSize: 20,
  },
  chosenNumber: {
    marginTop: 10,
    alignItems: 'center',
    width: '50%',
  },
});

export default StartGameScreen;
