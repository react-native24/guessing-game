# Guessing Game

## What is it about?

That's a simple guessing game on which the computer has to guess what number was chosen by the player.
The project was built in React Native what allows us to run it in crossover platforms either iOS or Android.

## Runing the project

- npm start

## Runing the tests

Under construction yet.

## Deployment

Under construction yet.

## Some screenshots

<b>Main screen</b>

<img src='main.jpg' width='250'>
