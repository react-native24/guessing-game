import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = (props: any) => {
  return (
    <View style={{ ...style.card, ...props['style'] }}>{props.children}</View>
  );
};

const style = StyleSheet.create({
  card: {
    padding: 15,
    borderRadius: 10,
    width: '80%',
    alignItems: 'center',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    shadowOpacity: 0.26,
    backgroundColor: 'white',
    // Only for Android devices
    elevation: 5,
  },
});

export default Card;
