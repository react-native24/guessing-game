export default {
  primary: '#ff8f00',
  secondary: '#c51162',
  tertiary: '#fd558f',
};
